function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

app = angular.module('anticorrupcion', ['ngMaterial', 'firebase','chart.js']);

app.factory('global', function($http) {
    return {
        info:  $http.get('/config/config.json')
        }
    });

app.filter('dateToISO', function() {
    return function(input) {
        return new Date(input).toISOString();
    };
});

app.filter('dateDiff',function () {
   return function (dateStart, dateEnd) {
        var start = moment(dateStart);
        var end = moment(dateEnd);

       return end.diff(start,"days");
   }
});


app.controller("login", function ($scope, $firebaseObject,global,$mdDialog){
    $scope.user = {};
     global.info.then(function (data) {
         $scope.global = data.data;
     });


    $scope.ingreso = function () {

        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(function() {
                return firebase.auth().signInWithEmailAndPassword($scope.user.usuario, $scope.user.pass)
                    .then(function (response) {
                        location.href = "/home/home.html";
                    });
            })
            .catch(function(error) {

            });

    }
});


app.controller("home", function ($scope, $firebaseObject, global,$mdDialog,$http,$mdSidenav){
    $scope.user = {};

    $scope.contrato = { };

    $scope.contratoSeleccionado ;

    $scope.estasObservaciones=[];

    $scope.nuevoItem = { };

    $scope.lista = [];

    $scope.alertas = [];

    global.info.then(function (data) {
        $scope.global = data.data;
    });


    $scope.save = function () {
        var ref = firebase.database().ref("contratos");

        ref.push($scope.contrato);
        $scope.contrato = {};
    }


    var query = firebase.database().ref("contratos").orderByKey();

    query.on("value",function(snapshot) {
            $scope.lista = [];
            snapshot.forEach(function(childSnapshot) {
                $scope.lista.push( childSnapshot);
                $scope.analizaAlertas(childSnapshot);
            });

            console.log($scope.lista)
        });
    
    $scope.loadDialogNew = function (ev) {
        $mdDialog.show({
            contentElement: '#myDialog',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        });

    }

    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    
    $scope.buscarSecop = function () {
        $http.get('https://www.datos.gov.co/resource/9f3w-id9a.json?proceso_de_compra='+$scope.filter.no_proceso)
            .then(function (data) {
                $scope.contrato = data.data[0];
            });
    }
    
    $scope.addItem = function (contrato,ev) {
        $scope.contratoSeleccionado = contrato;


        key = $scope.contratoSeleccionado.key;

        var ref = firebase.database().ref("contratos/"+key+"/observaciones");
        ref.on('value', function(snapshot) {
            $scope.estasObservaciones=[];
            snapshot.forEach(function(childSnapshot) {
                 $scope.estasObservaciones.push(childSnapshot);
            });
        });

        $mdDialog.show({
            contentElement: '#myDialog2',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        });
    }
    
    $scope.addObservacion = function () {

       key = $scope.contratoSeleccionado.key;

       //auxFecha = $scope.nuevoItem.fecha;

        //$scope.nuevoItem.fecha = (new Date(auxFecha)).getTime()

        var ref = firebase.database().ref("contratos/"+key+"/observaciones");

        ref.push($scope.nuevoItem);

        $scope.nuevoItem = {};
    }


    $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId) {
        return function() {
            $mdSidenav(componentId).toggle();
        };
    }


    $scope.openModule =  function (module) {
        location.href = "/home/"+module;
    }
    
    
    $scope.analizaAlertas= function (contrato) {
        var cantidadAlmuerzos = contrato.toJSON().almuerzo_cantidad;
        var cantidadRefrigerios = contrato.toJSON().refrigerio_cantidad;
        var fechaInicio = contrato.toJSON().fecha_de_inicio_del_contrato;
        var fechaFin = contrato.toJSON().fecha_de_fin_del_contrato;
        var totalPresupuesto =  contrato.toJSON().valor_del_contrato;
        var valorAlmuerzo =  contrato.toJSON().almuerzo_valor;
        var valorRefrigerio =  contrato.toJSON().refrigerio_valor;

        var diasTotales = moment(fechaFin).diff(fechaInicio,"days");

        key = contrato.key;

        var ref = firebase.database().ref("contratos/"+key+"/observaciones");

        var iterarObservaciones = [];

        ref.on('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {

                var estaobsercacion = childSnapshot.toJSON();

                iterarObservaciones.push(childSnapshot.toJSON());

                var esta_fecha = moment(estaobsercacion.fecha);

                var diasTranscurridos = moment(esta_fecha).diff(fechaInicio,"days");

                var fraccion_dias = diasTranscurridos/diasTotales;

                var fraccion_presupuesto_esperado = totalPresupuesto*fraccion_dias;

                var presupuesto_ejecutado_real  = (estaobsercacion.cantidad_almuerzos * valorAlmuerzo) + (estaobsercacion.cantidad_refrigerios * valorRefrigerio);



                $scope.alertas.push({
                    "dias_trasncurridos": diasTranscurridos,
                    "dias_totales" : diasTotales,
                    "total_presupuesto" : totalPresupuesto,
                    "fraccion_dias" : fraccion_dias,
                    "fraccion_presupuesto_esperado" :  fraccion_presupuesto_esperado,
                    "presupuesto_real_ejecutado" : presupuesto_ejecutado_real

                });

                $scope.values = [fraccion_presupuesto_esperado,presupuesto_ejecutado_real];
                $scope.labels =  ["Esperado", "Ejecutado"];

                $scope.options = {
                    responsive: true,
                    legend: {
                        display: true,
                    },
                    tooltips : {
                        enabled : true
                    },
                    elements: {
                        arc: {
                            borderWidth: 0
                        }
                    }
                }

                $scope.colors = [getRandomColor(),getRandomColor()];


                var ctx = document.getElementById('myChart').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: $scope.values,
                            backgroundColor:  $scope.colors
                        }],
                        labels: $scope.labels
                    },

                    options: $scope.options
                });


            });
        });

    }
    

});